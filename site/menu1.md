@def title = "Announcements"

# Announcements

![Alt Text](/assets/news.jpg)


## Week 15

* All the HW sets are opened until May 3rd.
* HW 11 is posted. Due on Monday, May 3rd.

## Week 13

* HW 10 is posted. Due on Friday, April 23rd.


## Week 12

* Quiz 7 is posted, due on Friday, April 9th. 
* HW 9 is posted. Due on Wednesday, April 14th.


## Week 10

* HW 7 is posted. Due on Wednesday, March 10th.
* Quiz 6 is posted, due on Friday, March 26th. 

## Week 9 

* HW 6 is due on Wednesday, March  24th. 


## Week 8 

* Quiz 5 is posted, due on Friday, March 12th. 


## Week 7 

* Extra credit problem set is avaialbe in Blackboard. Due on Monday, 03/08/2021 at 11:59PM.
* HW 5 is due on Wednesday, March 10th. 


## Week 5 

* HW 4 is due on Thursday, February 19th. 



## Week 4 

* Friday (02/12/2021) lecture delivers via ZOOM only. 
* Quiz 4 is posted in WebWork. Due on Friday, Feruary 12th.
* HW 4 is posted in WeBWork.  Due on Wednesday, February 17th. 

## Week 3 

* Quiz 3 is posted in WeBWork: Due on Friday, February 5th at 11:59PM (CST).
* HW 3 is posted in WeBWork: Due on Monday, February 8th. 

## Week 2 

* Quiz 2 is posted in WeBWork: Due on Friday, January 29th.
* HW 2 is posted in WeBWork: Due on Monday, February 1st. 


## Week 1 

* Install [Proctorio Chrome Extension](https://getproctorio.com/) 
     @@colbox-blue
     [Proctorio Quick Start Guide](https://drive.google.com/file/d/1L3AUF83Pq3Wz2aVIw6mVoTYKpWG1uEc7/view?usp=sharing)
     @@
* [Quiz 1 lives here!](https://forms.gle/NZMhzpFpVgPyip7Q9) Due on Friday, January 22nd.
* HW 1 is posted in WeBWork: Due on Wednesday, January 27th. 




