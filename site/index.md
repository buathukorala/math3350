@def title = "Welcome"
@def tags = ["syntax", "code"]

# Welcome to the World of Differential Equations! 

 
<!-- \tableofcontents  you can use \toc as well -->

**_MATH 3350: Most important math class you will ever take!_**

![Alt Text](/assets/toronto.gif)



---

[Anonymous Lecture Feedback](https://forms.gle/sf39wdC7xM1C9ahz9)

---


* Head over to the [WebWork](https://webwork.math.ttu.edu/webwork2/spr21bathukorm2450s012/)
* Head over to the [Blackboard](https://ttu.blackboard.com/)

---

## Things to be known:

Final Exam (cumulative): Friday, May 7th, 7:30 a.m. to 10:00 a.m. (CDT) via Blackboard + Proctorio

Final Exam info [lives here!](https://tinyurl.com/d8nbbxzx)

---

Course syllabus [lives here!](https://www.amazon.com/clouddrive/share/SnhcGfN2DIU12kufUKfpvJUhPUatteXOSVncqF8FZbI)


* **Lecture:** MWF: 9:00-9:50, HOLDEN 104 
    - Live class session via ZOOM: [Click here!!!](https://zoom.us/j/8065430626) 
* **Instructor:** Bhagya Athukorallage, PhD
* **Office Hours through Zoom:**  MW: 10:00am - 10:45am & MWF: 2:00pm - 3:00pm or by appointments
    - [Click here to enter my ZOOM office](https://zoom.us/j/8065430626)
* **Office:** MATH 18C
* **E-mail:** bhagya.athukorala@ttu.edu








