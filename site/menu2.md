@def title = "Class Notes"

# Class Notes

![Alt Text](/assets/notes3.jpg)

## Week 16

* Tuesday, May 4th
    - [Review session](https://tinyurl.com/2wktmrv7)
    - Missed the review, No worries (sorry, I forgot to record the first 15 minutes) [click here](https://youtu.be/mobbm5aXvO0)

* Monday, May 3rd
    - [Lecture note: Chap 5.1 (part2)](https://tinyurl.com/n9upv8kj)
    - Missed the lecture, No worries [click here](https://youtu.be/CbIzGQXMlOs)

## Week 15

* Friday, Apr. 30th
    - [Lecture note: Chap 5.1 (part1)](https://tinyurl.com/n9upv8kj)
    - Missed the lecture, No worries [click here](https://youtu.be/dVeH1-jkBMQ)

* Wednesday, Apr. 28th
    - [Lecture note: Chap 4.5](https://tinyurl.com/ttrrdd5f)
    - Missed the lecture, No worries [click here](https://youtu.be/dEydqIdbDWU)

* Monday, Apr. 26th
    - [Lecture note: Chap 4.3 (part3)](https://tinyurl.com/2x23xaam)
    - Missed the lecture, No worries [click here](https://youtu.be/7MUzf2qkszg)

## Week 14

* Friday, Apr. 23rd
    - [Lecture note: Chap 4.4 (part2)](https://tinyurl.com/495swrd5)
    - Missed the lecture, No worries [click here](https://youtu.be/hr-Nvm9wQIE)

* Wednesday, Apr. 21st
    - [Lecture note: Chap 4.4 (part1)](https://tinyurl.com/58c4azet)
    - Missed the lecture, No worries [click here](https://youtu.be/cEyLJhvN124)

* Monday, Apr. 19th
    - [Lecture note: Chap 4.3 (part2)](https://tinyurl.com/4b969ccy)
    - Missed the lecture, No worries [click here](https://youtu.be/ltY7POjLD90)


## Week 13

* Friday, Apr. 16th
    - Exam 2 
    - Missed the review, No worries[click here](https://youtu.be/ylLEIraLATI)

* Wednesday, Apr. 14th
    - [Lecture note: Chap 4.3 (part1)](https://tinyurl.com/z32fs3jd)
    - Missed the lecture, No worries [click here](https://youtu.be/Is67d1-lPfc)

* Monday, Apr. 12th
    - [Lecture note: Chap 4.2 (part2)](https://tinyurl.com/3nvr6ryn)
    - Missed the lecture, No worries [click here - starts at 11:00 min](https://youtu.be/TY73lyLGs7w)


## Week 12

* Friday, Apr. 9nd
    - [Lecture note: Chap 4.1 (part2) and 4.2 (part1)](https://tinyurl.com/3wpu258r)
    - Missed the lecture, No worries [click here](https://youtu.be/PBskLY6fgrg)

* Wednesday, Apr. 7th
    - [Lecture note: Chap 4.1 (part1)](https://tinyurl.com/bmp4zp56)
    - Missed the lecture, No worries [click here](https://youtu.be/yZ0XXcIf42I)


## Week 11

* Friday, Apr. 2nd
    - [Lecture note: Chap 3.7 (part2)](https://tinyurl.com/apzf95jk)
    - Missed the lecture, No worries [click here](https://youtu.be/93oJP51q12A)

* Wednesday, Mar. 31st

    - [Lecture note: Ch 3.7 (part1)](https://tinyurl.com/mn4xcbyw)
    - Missed the lecture, No worries [click here](https://youtu.be/9EVP8Y0vnRw)

* Monday, Mar. 29th

    - [Lecture note: Chap 3.7 (part1)](https://tinyurl.com/sxr38ewm)


## Week 10 

* Friday, Mar. 26th
    - [Lecture note: Chap 3.6](https://tinyurl.com/yw773d5n)
    - Missed the lecture, No worries [click here](https://youtu.be/WcKFlRe4NSg)

* Wednesday, Mar. 24th

    - [Lecture note: Ch 3.5 (complete)](https://tinyurl.com/5yrj47a7)
    - Missed the lecture, No worries [click here](https://youtu.be/b4DCZGe0Cp4)

* Monday, Mar. 22nd

    - [Lecture note: Chap 3.5 (part1)](https://tinyurl.com/rpftdtky)
    - [Lecture note: Chap 3.4 (complete)](https://tinyurl.com/225srzmh)
    - Missed the lecture, No worries [click here](https://youtu.be/4rLlYZjiTYI)

## Week 9


* Wednesday, Mar. 17th

    - [Lecture note: Ch 3.4 (complete)](https://tinyurl.com/225srzmh)
    - Missed the lecture, No worries [click here](https://youtu.be/H9Tqczt_SeE)

* Monday, Mar. 15th
    - [Lecture note: Chap 3.4 (part1)](https://tinyurl.com/2nd4yej4)
    - Missed the lecture, No worries [click here](https://youtu.be/KCstgIiU5R8)


## Week 8 


* Friday, Mar. 12th
    - [Lecture note: Chap 3.2](https://tinyurl.com/z2yufr9f)
    - Missed the lecture, No worries[click here](https://youtu.be/3qme3vm7BhQ)


* Wednesday, Mar. 3rd

    - [Lecture note: Ch 3.3 (complete)](https://tinyurl.com/4xezc6pu)
    - Missed the lecture, No worries [click here](https://youtu.be/V6efFixN39o)

* Monday, Mar. 8th
    - [Lecture note: Chap 3.3 (part2)](https://tinyurl.com/42r9bv8r)
    - Missed the lecture, No worries [click here](https://youtu.be/rjtkgI3_qTA)


## Week 7

* Friday, Mar. 5th
    - [Lecture note: Chap 3.3 (part2)](https://tinyurl.com/42r9bv8r)
    - Missed the lecture, No worries[click here](https://youtu.be/99PyKk1_32o)
    - Special thanks to Dr. Bornia for covering the class


* Wednesday, Mar. 3rd

    - [Lecture note: Ch 3.3 (part1)](https://tinyurl.com/7bf9yt6r)
    - [Lecture note: Ch 3.1 (complete)](https://tinyurl.com/56hav3wp)
    - Missed the lecture, No worries [click here](https://youtu.be/PVLGKJIRrbs)

* Monday, Mar. 1st
    - [Lecture note: Ch 3.1 (part1)](https://tinyurl.com/nwk2uecy)
    - Missed the lecture, No worries [click here](https://youtu.be/-XaZ4Cm5GE4)



## Week 6

* Monday, Feb. 22nd
    - [Exam 1 - review (part2)](https://tinyurl.com/ywz5nzrd)
    - Missed the lecture, No worries [click here](https://youtu.be/QUNtWamer-k)



## Week 5 

* Friday, Feb. 19th

    - [Exam 1 - review (part1)](https://tinyurl.com/148jtx39)
    - [Lecture note: Ch 2.6 (complete)](https://tinyurl.com/ymgv04n1)
    - Missed the lecture, No worries [click here](https://youtu.be/6ngPPfo02f4)

* Wednesday, Feb. 17th

    - [Lecture note: Ch 2.6 (part1)](https://tinyurl.com/1ce325k1)
    - [Lecture note: Ch 2.5 (complete)](https://tinyurl.com/1gyng6c5)
    - Missed the lecture, No worries [click here](https://youtu.be/Hgpzygh5y3c)

* Monday, Feb. 15th
    - [Lecture note: Ch 2.5 (part2)](https://tinyurl.com/3ow4fhdo)
    - Missed the lecture, No worries [click here](https://youtu.be/39RBliE67aA)

## Week 4 

* Friday, Feb. 12th
    - [Lecture note: Ch 2.5(part1)](https://tinyurl.com/1h6skgyr)
    - Missed the lecture, No worries[click here](https://youtu.be/lXKO8j66Nh0)

* Wednesday, Feb. 10th
    - [Lecture note: Ch 2.4 (complete)](https://tinyurl.com/1fy7ick5)
    - Missed the lecture, No worries [click here](https://youtu.be/ZlMvlT36ib0)

* Monday, Feb. 8th
    - [Lecture note: Ch 2.4 (part1)](https://tinyurl.com/3m3vfh4y)
    - Missed the lecture, No worries[click here](https://youtu.be/fVcWUmwamcM)

## Week 3 

* Friday, Feb. 5th
    - [Lecture note: Ch 2.3 (complete)](https://tinyurl.com/42mwvybe)
    - Missed the lecture, No worries - sorry, 1st half is missing [click here](https://youtu.be/UoQQbAAIHbU)

* Wednesday, Feb. 3rd
    - [Lecture note: Ch 2.3 (part1)](https://tinyurl.com/rkkhlkhu)
    - [Lecture note: Ch 2.2 (complete)](https://tinyurl.com/2vr57bpv)
    - Missed the lecture, No worries [click here](https://youtu.be/QU92OfA459w)

* Monday, Feb. 1st
    - [Lecture note: Ch 2.2](https://tinyurl.com/2vr57bpv)
    - Missed the lecture, No worries [click here](https://youtu.be/-Mq9LOfqsNg)


## Week 2 

* Friday, Jan. 29th
    - [Lecture note: Ch 2.1](https://tinyurl.com/y3lqo3tq)
    - Missed the lecture, No worries [click here](https://youtu.be/dQO1rWItsKQ)

* Wednesday, Jan. 27th
    - [Lecture note: Ch 1.2 (part2)](https://tinyurl.com/y272eknx)
    - Missed the lecture, No worries [click here](https://youtu.be/BTcKErMsNK0)

* Monday, Jan. 25th
    - [Lecture note: Ch 1.1 (part2) and Ch 1.2 (part1)](https://tinyurl.com/y6436xzv)
    - Missed the lecture, No worries [click here](https://youtu.be/cZlls-ATuh4)

## Week 1 

* Friday, Jan. 22nd
    - [Lecture note: Ch 1.1 (part1)](https://www.amazon.com/clouddrive/share/5pdyN2jrJwyVIcU3g31qIjSi1CIBjBLv6rjYnSUitKz)
    - Missed the lecture, No worries [click here](https://youtu.be/BaR0k2QHnjo?t=20)

* Wednesday, Jan. 20th
    - [Welcome slides!](https://www.amazon.com/clouddrive/share/CrobtmM4iqn5rdn2wjt15qHQX4zwT8IBd0emex5j0Oh)
  


