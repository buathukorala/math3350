@def title = "WebWork"

# Homework

![Alt Text](/assets/HW.jpg)


## WebWork assignments Open, closes 04/23/2021 at 11:59pm CDT.

* [HW 11](https://webwork.math.ttu.edu/webwork2/spr21bathukorm3350sDC1s111/) is posted. Due on Monday, May 3rd. 
* [HW 10](https://webwork.math.ttu.edu/webwork2/spr21bathukorm3350sDC1s111/) is posted. Due on Wednesday, April 28th.  
* [HW 9](https://webwork.math.ttu.edu/webwork2/spr21bathukorm3350sDC1s111/) is posted. Due on Wednesday, April 14th.  
* [HW 8](https://webwork.math.ttu.edu/webwork2/spr21bathukorm3350sDC1s111/) is posted. Due on Wednesday, April 7th.  
* [HW 7](https://webwork.math.ttu.edu/webwork2/spr21bathukorm3350sDC1s111/) is posted. Due on Wednesday, March 31st.  
* [HW 6](https://webwork.math.ttu.edu/webwork2/spr21bathukorm3350sDC1s111/) is posted. Due on Wednesday, March 24th.  
* [HW 5](https://webwork.math.ttu.edu/webwork2/spr21bathukorm3350sDC1s111/) is posted. Due on Wednesday, March 10th.  
* [HW 4](https://webwork.math.ttu.edu/webwork2/spr21bathukorm3350sDC1s111/) is posted. Due on Thursday, Feb. 18th.  
* [HW 3](https://webwork.math.ttu.edu/webwork2/spr21bathukorm3350sDC1s111/) is posted. Due on Monday, Feb. 8th. 
* [HW 2](https://webwork.math.ttu.edu/webwork2/spr21bathukorm3350sDC1s111/) is posted. Due on Monday, Feb. 1st. 
* [HW 1](https://webwork.math.ttu.edu/webwork2/spr21bathukorm3350sDC1s111/) is posted. Due on Wednesday, Jan. 27th. 


## Useful WeBWork Info

* Head over to the [WebWork](https://webwork.math.ttu.edu/webwork2/spr21bathukorm3350sDC1s111/) 
* [Click here](https://www.amazon.com/clouddrive/share/LYteFM8qahwmYBIX977VIVzz0QLglHdl1LIMCRKa0BG) to see a tutorial on how to use WeBWork
* [Click here](https://www.amazon.com/clouddrive/share/z6xFHyYgtF62mzT03buAib4NP2L7seZoWDyqBoBTFKQ) to read the tutorial on entering answers in WeBWork






